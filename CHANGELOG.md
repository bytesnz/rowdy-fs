# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2019-04-29

### Added
- Ability to return a Promise from a modify function
_ Ability to pause and then resume a queue on an error

### Changed
- Updated dependencies

## [1.0.0] - 2019-01-28
Initial Release

[1.1.0]: https://gitlab.com/bytesnz/rowdy-fs/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/bytesnz/rowdy-fs/tree/v1.0.0
