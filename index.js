"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
const fs = require("fs");
const util_1 = require("util");
const readFile = util_1.promisify(fs.readFile);
const writeFile = util_1.promisify(fs.writeFile);
const appendFile = util_1.promisify(fs.appendFile);
;
class Queue {
    constructor(filename, options = {}) {
        this.queue = Promise.resolve();
        this.lastWasRead = false;
        this.lastError = null;
        this.filename = path_1.resolve(filename);
        this.options = options;
        this.pauseOnError = this.pauseOnError.bind(this);
        this.writeNow = this.writeNow.bind(this);
        this.appendNow = this.appendNow.bind(this);
        this.modifyNow = this.modifyNow.bind(this);
    }
    pauseOnError(error) {
        // if (this.lastError) {
        return new Promise((resolve, reject) => {
            this.lastError = {
                error,
                resolve: () => {
                    this.lastError = null;
                    resolve();
                },
                reject
            };
        });
    }
    /**
     * Read the selected file and catch a ENOENT error if the file doesn't
     * already exist
     */
    readAndCatch() {
        return readFile(this.filename).catch((error) => {
            if (error.code === 'ENOENT') {
                return Promise.resolve(Buffer.from(''));
            }
            return Promise.reject(error);
        });
    }
    /**
     * Clear current error state. If an error has occured and modifications
     * need to be done to the file before the queue is continued, the `Now`
     * functions should be used to carry out the modification and then
     * `clearError` called to clear the error and resume the queue.
     *
     * @returns A Promise that will resolve once the queue is completed
     */
    clearError() {
        if (this.lastError) {
            this.lastError.resolve();
            this.lastError = null;
        }
        return this.queue;
    }
    /**
     * Get the current error
     *
     * @returns The current error or null if no current error
     */
    currenError() {
        if (this.lastError) {
            return this.lastError.error;
        }
        return null;
    }
    /**
     * Replace the current content of the file with given content
     *
     * @param content Content to replace the exisiting content with
     *
     * @returns A Promise that resolves once the write is complete
     */
    write(content) {
        if (this.lastError) {
            return Promise.reject(this.lastError.error);
        }
        const newQueue = this.queue.then(() => this.writeNow(content));
        this.lastWasRead = false;
        if (this.options.pauseOnWriteError) {
            this.queue = newQueue.catch(this.pauseOnError);
            return newQueue;
        }
        else {
            this.queue = newQueue;
            return this.queue;
        }
    }
    /**
     * Replaces the current content of the file with the given content now
     * without any regard for the queue.
     *
     * Warning: This should only be used when the queue is paused due to an error
     *
     * @param content Content to replace the exisiting content with
     *
     * @returns A Promise that resolves once the write is complete
     */
    writeNow(content) {
        return writeFile(this.filename, content);
    }
    /**
     * Append a string to the end of the file
     *
     * @param content Content to append to the end of the file
     *
     * @returns A Promise that resolves once the append is complete
     */
    append(content) {
        if (this.lastError) {
            return Promise.reject(this.lastError.error);
        }
        const newQueue = this.queue.then(() => this.appendNow(content));
        this.lastWasRead = false;
        if (this.options.pauseOnWriteError) {
            this.queue = newQueue.catch(this.pauseOnError);
            return newQueue;
        }
        else {
            this.queue = newQueue;
            return this.queue;
        }
    }
    /**
     * Append a string to the end of the file now without any regard for the
     * queue.
     *
     * Warning: This should only be used when the queue is paused due to an error
     *
     * @param content Content to append to the end of the file
     *
     * @returns A Promise that resolves once the append is complete
     */
    appendNow(content) {
        return appendFile(this.filename, content);
    }
    /**
     * Modify the contents of a file using the provided function
     *
     * @param modifyFunction Modify function to call once the content has been
     *   retrieved
     *
     * @returns A Promise that resolves once the modification has been completed
     */
    modify(modifyFunction) {
        if (this.lastError) {
            return Promise.reject(this.lastError.error);
        }
        const newQueue = this.queue.then((buffer) => this.modifyNow(modifyFunction, buffer));
        this.lastWasRead = false;
        if (this.options.pauseOnWriteError) {
            this.queue = newQueue.catch(this.pauseOnError);
            return newQueue;
        }
        else {
            this.queue = newQueue;
            return this.queue;
        }
    }
    /**
     * Modify the contents of a file using the provided function
     *
     * Warning: This should only be used when the queue is paused due to an error
     *
     * @param modifyFunction Modify function to call once the content has been
     *   retrieved
     * @param content Current content. Should not normally be given
     *
     * @returns A Promise that resolves once the modification has been completed
     */
    modifyNow(modifyFunction, content) {
        let promise;
        if (content instanceof Buffer) {
            promise = Promise.resolve(content);
        }
        else {
            promise = this.readAndCatch();
        }
        return promise.then((buffer) => {
            const modifyResult = modifyFunction(buffer);
            if (modifyResult instanceof Promise) {
                return modifyResult.then((newContent) => writeFile(this.filename, newContent));
            }
            else {
                return writeFile(this.filename, modifyResult);
            }
        });
    }
    /**
     * Read the contents of the file
     */
    read() {
        if (this.lastError) {
            return Promise.reject(this.lastError.error);
        }
        if (this.lastWasRead) {
            return this.queue;
        }
        this.queue = this.queue.then((buffer) => {
            if (buffer instanceof Buffer) {
                return buffer;
            }
            else {
                return this.readAndCatch();
            }
        });
        this.lastWasRead = true;
        return this.queue;
    }
}
exports.default = Queue;
//# sourceMappingURL=index.js.map