"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_monkey_1 = require("fs-monkey");
const memfs_1 = require("memfs");
const unionfs_1 = require("unionfs");
const fsReal = require("fs");
exports.vol = new memfs_1.Volume();
exports.fs = memfs_1.createFsFromVolume(exports.vol);
const fs2 = Object.assign({}, fsReal);
unionfs_1.ufs.use(fs2).use(exports.vol);
fs_monkey_1.patchFs(unionfs_1.ufs);
//# sourceMappingURL=unionfs.js.map