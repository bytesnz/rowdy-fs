import { resolve }  from 'path';
import * as fs from 'fs';
import { promisify } from 'util';

const readFile = promisify(fs.readFile);
const writeFile = promisify(fs.writeFile);
const appendFile = promisify(fs.appendFile);

interface Options {
  pauseOnWriteError?: boolean
}

interface FileAccessError extends Error {
  code: string
};

interface LastError {
  error: Error,
  resolve: (value?: Buffer) => void,
  reject: (error?: Error) => void
}

class Queue {
  /// Filename the queue is for
  protected filename: string;
  protected options: Options;
  protected queue: Promise<Buffer | void> = Promise.resolve();
  protected lastWasRead: boolean = false;
  protected lastError: LastError | null = null;

  constructor (filename: string, options: Options = {}) {
    this.filename = resolve(filename);
    this.options = options;

    this.pauseOnError = this.pauseOnError.bind(this);
    this.writeNow = this.writeNow.bind(this);
    this.appendNow = this.appendNow.bind(this);
    this.modifyNow = this.modifyNow.bind(this);
  }

  protected pauseOnError (error): Promise<void | Buffer> {
    // if (this.lastError) {
    return new Promise((resolve, reject) => {
      this.lastError = {
        error,
        resolve: () => {
          this.lastError = null;
          resolve();
        },
        reject
      };
    });
  }

  /**
   * Read the selected file and catch a ENOENT error if the file doesn't
   * already exist
   */
  protected readAndCatch (): Promise<Buffer> {
    return readFile(this.filename).catch((error: FileAccessError) => {
      if (error.code === 'ENOENT') {
        return Promise.resolve(Buffer.from(''));
      }

      return Promise.reject(error);
    });
  }

  /**
   * Clear current error state. If an error has occured and modifications
   * need to be done to the file before the queue is continued, the `Now`
   * functions should be used to carry out the modification and then
   * `clearError` called to clear the error and resume the queue.
   *
   * @returns A Promise that will resolve once the queue is completed
   */
  clearError (): Promise<void | Buffer> {
    if (this.lastError) {
      this.lastError.resolve();
      this.lastError = null;
    }
    return this.queue;
  }

  /**
   * Get the current error
   *
   * @returns The current error or null if no current error
   */
  currenError (): Error | null {
    if (this.lastError) {
      return this.lastError.error;
    }

    return null;
  }

  /**
   * Replace the current content of the file with given content
   *
   * @param content Content to replace the exisiting content with
   *
   * @returns A Promise that resolves once the write is complete
   */
  write (content: string | Buffer): Promise<void> {
    if (this.lastError) {
      return Promise.reject(this.lastError.error);
    }

    const newQueue = this.queue.then(() => this.writeNow(content));
    this.lastWasRead = false;
    if (this.options.pauseOnWriteError) {
      this.queue = newQueue.catch(this.pauseOnError);
      return newQueue;
    } else {
      this.queue = newQueue;
      return <Promise<void>>this.queue;
    }
  }

  /**
   * Replaces the current content of the file with the given content now
   * without any regard for the queue.
   *
   * Warning: This should only be used when the queue is paused due to an error
   *
   * @param content Content to replace the exisiting content with
   *
   * @returns A Promise that resolves once the write is complete
   */
  writeNow (content: string | Buffer): Promise<void> {
    return writeFile(this.filename, content);
  }

  /**
   * Append a string to the end of the file
   *
   * @param content Content to append to the end of the file
   *
   * @returns A Promise that resolves once the append is complete
   */
  append (content: string | Buffer): Promise<void> {
    if (this.lastError) {
      return Promise.reject(this.lastError.error);
    }

    const newQueue = this.queue.then(() => this.appendNow(content));
    this.lastWasRead = false;
    if (this.options.pauseOnWriteError) {
      this.queue = newQueue.catch(this.pauseOnError);
      return newQueue;
    } else {
      this.queue = newQueue;
      return <Promise<void>>this.queue;
    }
  }

  /**
   * Append a string to the end of the file now without any regard for the
   * queue.
   *
   * Warning: This should only be used when the queue is paused due to an error
   *
   * @param content Content to append to the end of the file
   *
   * @returns A Promise that resolves once the append is complete
   */
  appendNow (content: string | Buffer): Promise<void> {
    return appendFile(this.filename, content);
  }

  /**
   * Modify the contents of a file using the provided function
   *
   * @param modifyFunction Modify function to call once the content has been
   *   retrieved
   *
   * @returns A Promise that resolves once the modification has been completed
   */
  modify (modifyFunction: (content: Buffer) => (string | Buffer | Promise<string | Buffer>)): Promise<void> {
    if (this.lastError) {
      return Promise.reject(this.lastError.error);
    }

    const newQueue = this.queue.then((buffer) => this.modifyNow(modifyFunction, buffer));
    this.lastWasRead = false;

    if (this.options.pauseOnWriteError) {
      this.queue = newQueue.catch(this.pauseOnError);
      return newQueue;
    } else {
      this.queue = newQueue;
      return <Promise<void>>this.queue;
    }
  }

  /**
   * Modify the contents of a file using the provided function
   *
   * Warning: This should only be used when the queue is paused due to an error
   *
   * @param modifyFunction Modify function to call once the content has been
   *   retrieved
   * @param content Current content. Should not normally be given
   *
   * @returns A Promise that resolves once the modification has been completed
   */
  modifyNow (modifyFunction: (content: Buffer) => (string | Buffer | Promise<string | Buffer>), content?: void | Buffer): Promise<void> {
    let promise;

    if (content instanceof Buffer) {
      promise = Promise.resolve(content);
    } else {
      promise = this.readAndCatch();
    }

    return promise.then((buffer) => {
      const modifyResult = modifyFunction(buffer);
      if (modifyResult instanceof Promise) {
        return modifyResult.then((newContent) => writeFile(this.filename, newContent));
      } else {
        return writeFile(this.filename, modifyResult);
      }
    });
  }

  /**
   * Read the contents of the file
   */
  read (): Promise<Buffer> {
    if (this.lastError) {
      return Promise.reject(this.lastError.error);
    }

    if (this.lastWasRead) {
      return <Promise<Buffer>>this.queue;
    }

    this.queue = this.queue.then((buffer) => {
      if (buffer instanceof Buffer) {
        return buffer;
      } else {
        return this.readAndCatch();
      }
    });
    this.lastWasRead = true;

    return <Promise<Buffer>>this.queue;
  }

  /**
   * Return a Promise that will resolve once all operations have been completed
   */
}
export default Queue;
