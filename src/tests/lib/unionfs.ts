import { patchFs } from 'fs-monkey';
import { Volume, createFsFromVolume } from 'memfs';
import { ufs } from 'unionfs';
import * as fsReal from 'fs';
export const vol = new Volume();
export const fs = createFsFromVolume(vol);
const fs2 = Object.assign({}, fsReal);
ufs.use(fs2).use(vol);
patchFs(ufs);
