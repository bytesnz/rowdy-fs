import test from 'ava';

import { fs as testFs } from './tests/lib/unionfs';

import * as fs from 'fs';
import { promisify } from 'util';

const readFile = promisify(fs.readFile);
const writeFile = promisify(fs.writeFile);
const appendFile = promisify(fs.appendFile);
const mkdir = promisify(testFs.mkdir);

test.before((t) => {
  return mkdir('/test');
});

import Queue from './index';

test('The Queue writes to the file when requested', (t) => {
  const oldFile = '/test/test1';
  const newFile = '/test/testnew1';
  const content = 'This is a test';
  const oldQueue = new Queue(oldFile);
  const newQueue = new Queue(newFile);
  return Promise.all([
    newQueue.write(content)
        .then(() => readFile(newFile)).then((buffer) => {
      t.is(content, buffer.toString());
    }),
    writeFile(oldFile, 'Old Content').then(() => oldQueue.write(content))
        .then(() => readFile(oldFile)).then((buffer) => {
      t.is(content, buffer.toString());
    })
  ]).then(() => {});
});

test('The Queue appends to the file when requested', (t) => {
  const oldFile = '/test/test2';
  const newFile = '/test/testnew2';
  const oldContent = 'Old Content';
  const content = 'This is a test';
  const oldQueue = new Queue(oldFile);
  const newQueue = new Queue(newFile);
  return Promise.all([
    newQueue.append(content)
        .then(() => readFile(newFile)).then((buffer) => {
      t.is(content, buffer.toString());
    }),
    writeFile(oldFile, oldContent).then(() => oldQueue.append(content))
        .then(() => readFile(oldFile)).then((buffer) => {
      t.is(oldContent + content, buffer.toString());
    })
  ]).then(() => {});
});

test('The Queue uses the modify function to edit the file when requested', (t) => {
  const oldFile = '/test/test3';
  const newFile = '/test/testnew3';
  const oldContent = 'Old Content';
  const content = 'This is a test';
  const oldQueue = new Queue(oldFile);
  const newQueue = new Queue(newFile);
  return Promise.all([
    newQueue.modify((current) => {
      t.is('', current.toString());
      return content;
    }).then(() => readFile(newFile)).then((buffer) => {
      t.is(content, buffer.toString());
    }),
    writeFile(oldFile, oldContent).then(() => oldQueue.modify((current) => {
      t.is(oldContent, current.toString());
      return content + oldContent.toString();
    })).then(() => readFile(oldFile)).then((buffer) => {
      t.is(content + oldContent, buffer.toString());
    })
  ]).then(() => {});
});

test('The Queue handles a modify function that returns a Promise to edit the file when requested', (t) => {
  const oldFile = '/test/test3';
  const newFile = '/test/testnew3';
  const oldContent = 'Old Content';
  const content = 'This is a test';
  const oldQueue = new Queue(oldFile);
  const newQueue = new Queue(newFile);
  return Promise.all([
    newQueue.modify((current) => {
      t.is('', current.toString());
      return Promise.resolve(content);
    }).then(() => readFile(newFile)).then((buffer) => {
      t.is(content, buffer.toString());
    }),
    writeFile(oldFile, oldContent).then(() => oldQueue.modify((current) => {
      t.is(oldContent, current.toString());
      return Promise.resolve(content + oldContent.toString());
    })).then(() => readFile(oldFile)).then((buffer) => {
      t.is(content + oldContent, buffer.toString());
    })
  ]).then(() => {});
});


test('The Queue pauses on an error when pauseOnWriteError set', (t) => {
  const oldFile = '/test/test7';
  const newFile = '/test/testnew7';
  const oldContent = 'Old Content';
  const content = 'This is a test';
  const oldQueue = new Queue(oldFile, { pauseOnWriteError: true });
  const newQueue = new Queue(newFile, { pauseOnWriteError: true });
  return writeFile(oldFile, oldContent).then(() => {
    return Promise.all([
      { queue: newQueue, finalContent: '' },
      { queue: oldQueue, finalContent: oldContent }
    ].map(({queue, finalContent}) => {
      return queue.modify((currentContent) => Promise.reject('bad'))
      .then(() => t.fail('Resolved when it should have rejected'))
      .catch((error) => {
        t.is('bad', error);

        // Test all operations reject when paused due to error
        return Promise.all([
          queue.read()
          .then(() => t.fail('Read should have rejected with paused on error'))
          .catch(() => Promise.resolve()),
          queue.write('')
          .then(() => t.fail('Write should have rejected with paused on error'))
          .catch(() => Promise.resolve()),
          queue.append('')
          .then(() => t.fail('Append should have rejected with paused on error'))
          .catch(() => Promise.resolve()),
          queue.modify((content) => content)
          .then(() => t.fail('Modify should have rejected with paused on error'))
          .catch(() => Promise.resolve())
        ]);
      }).then(() => {
        return queue.clearError();
      }).then(() => {
        return queue.read();
      }).then((content) => {
        t.is(finalContent, content.toString());
      });
    }));
  }).then(() => {});
});

test('The Queue reads to the file when requested', (t) => {
  const oldFile = '/test/test4';
  const newFile = '/test/testnew4';
  const oldContent = 'Old Content';
  const oldQueue = new Queue(oldFile);
  const newQueue = new Queue(newFile);
  return Promise.all([
    newQueue.read().then((buffer) => {
      t.is('', buffer.toString());
    }),
    writeFile(oldFile, oldContent).then(() => oldQueue.read())
        .then((buffer) => {
      t.is(oldContent, buffer.toString());
    })
  ]).then(() => {});
});

test('Multiple reads in a row get the same Promise', (t) => {
  const file = '/test/test5';
  const queue = new Queue(file);

  const promise1 = queue.read();
  const promise2 = queue.write('check');
  const promise3 = queue.read();
  const promise4 = queue.read();
  t.not(promise1, promise3);
  t.is(promise3, promise4);
});

test('The Queue carries out the queue in order', (t) => {
  const file = '/test/test6';
  const content1 = 'This is a';
  const content2 = 'line of tests';
  const content3 = 'another content';

  const queue = new Queue(file);

  return Promise.all([
    queue.write(content1),
    queue.read().then((content) => {
      t.is(content1, content.toString());
    }),
    queue.append(content2),
    queue.read().then((content) => {
      t.is(content1 + content2, content.toString());
    }),
    queue.modify((content) => {
      return content + content3;
    }),
    queue.read().then((content) => {
      t.is(content1 + content2 + content3, content.toString());
    })
  ]).then(() => {});
});

