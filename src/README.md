rowdy-fs <!=package version>
=========================================
[![Pipeline Status](https://gitlab.com/bytesnz/rowdy-fs/badges/master/pipeline.svg)](https://gitlab.com/bytesnz/rowdy-fs/commits/master)
[![MARSS on NPM](https://bytes.nz/b/rowdy-fs/npm)](https://npmjs.com/package/rowdy-fs)

<!=package description>

rowdy-fs is a single Queue class that queues writes, appends, modifications
and reads to a single file, so you don't have to worry about race
conditions

## Definition (Typescript)
```typescript
<!=include index.d.ts>
```

<!=include CHANGELOG.md>
