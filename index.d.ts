/// <reference types="node" />
interface Options {
    pauseOnWriteError?: boolean;
}
interface LastError {
    error: Error;
    resolve: (value?: Buffer) => void;
    reject: (error?: Error) => void;
}
declare class Queue {
    protected filename: string;
    protected options: Options;
    protected queue: Promise<Buffer | void>;
    protected lastWasRead: boolean;
    protected lastError: LastError | null;
    constructor(filename: string, options?: Options);
    protected pauseOnError(error: any): Promise<void | Buffer>;
    /**
     * Read the selected file and catch a ENOENT error if the file doesn't
     * already exist
     */
    protected readAndCatch(): Promise<Buffer>;
    /**
     * Clear current error state. If an error has occured and modifications
     * need to be done to the file before the queue is continued, the `Now`
     * functions should be used to carry out the modification and then
     * `clearError` called to clear the error and resume the queue.
     *
     * @returns A Promise that will resolve once the queue is completed
     */
    clearError(): Promise<void | Buffer>;
    /**
     * Get the current error
     *
     * @returns The current error or null if no current error
     */
    currenError(): Error | null;
    /**
     * Replace the current content of the file with given content
     *
     * @param content Content to replace the exisiting content with
     *
     * @returns A Promise that resolves once the write is complete
     */
    write(content: string | Buffer): Promise<void>;
    /**
     * Replaces the current content of the file with the given content now
     * without any regard for the queue.
     *
     * Warning: This should only be used when the queue is paused due to an error
     *
     * @param content Content to replace the exisiting content with
     *
     * @returns A Promise that resolves once the write is complete
     */
    writeNow(content: string | Buffer): Promise<void>;
    /**
     * Append a string to the end of the file
     *
     * @param content Content to append to the end of the file
     *
     * @returns A Promise that resolves once the append is complete
     */
    append(content: string | Buffer): Promise<void>;
    /**
     * Append a string to the end of the file now without any regard for the
     * queue.
     *
     * Warning: This should only be used when the queue is paused due to an error
     *
     * @param content Content to append to the end of the file
     *
     * @returns A Promise that resolves once the append is complete
     */
    appendNow(content: string | Buffer): Promise<void>;
    /**
     * Modify the contents of a file using the provided function
     *
     * @param modifyFunction Modify function to call once the content has been
     *   retrieved
     *
     * @returns A Promise that resolves once the modification has been completed
     */
    modify(modifyFunction: (content: Buffer) => (string | Buffer | Promise<string | Buffer>)): Promise<void>;
    /**
     * Modify the contents of a file using the provided function
     *
     * Warning: This should only be used when the queue is paused due to an error
     *
     * @param modifyFunction Modify function to call once the content has been
     *   retrieved
     * @param content Current content. Should not normally be given
     *
     * @returns A Promise that resolves once the modification has been completed
     */
    modifyNow(modifyFunction: (content: Buffer) => (string | Buffer | Promise<string | Buffer>), content?: void | Buffer): Promise<void>;
    /**
     * Read the contents of the file
     */
    read(): Promise<Buffer>;
}
export default Queue;
